<?php 
include_once('library/db_func.php');
include_once('library/controller.php');
session_start();

	// melakukan capture semua yang diecho/kirimkan
	ob_start();
	// cek modul // jika ada, file-nya, load controller, 
	// jika tidak, hanya echo kan selamat datang
	$modul = isset($_GET['modul'])?'modul/'.$_GET['modul'].'.php':'';
	if($modul!='' && is_file($modul)){
		include_once($modul);
	}
	else{
		echo "<h3>Selamat Datang di Portal Mahasiswa</h3>";
	}

	// simpan capture ke variabel content 
	$content =  ob_get_clean();

	// load menu:
	$menu = '<ul class="app-bar-menu">';
	$list_menu = array(
					'Home'=>'index.php',
					'Mata Kuliah'=>'index.php?modul=matkul',
					'Daftar Nilai'=>'index.php?modul=daftarnilai',
					'Jadwal Kuliah'=>'index.php?modul=jadwal',
					'Daftar Dosen'=>'index.php?modul=daftardosen',
					'Daftar Mahasiswa'=>'index.php?modul=daftarmhs',
				);
	if(isset($_SESSION['login']) && $_SESSION['login']!=''){
		$list_menu['Keluar('.$_SESSION['login'].')'] = 'index.php?modul=masuk&logout=1';
	}
	else{
		$list_menu['Masuk'] = 'index.php?modul=masuk';
	}
	
	foreach($list_menu as $k=>$y){
		$menu .='<li><a href="'.$y.'">'.$k.'</a></li>';
	}
	
	$menu .= '</ul>';
	
	// load index.php template, 
	// letakkan content di dalam index.php 
	include('template/metro.php');
?>
