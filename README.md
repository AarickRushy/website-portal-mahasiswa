## Tampilan User Interface Website

**Login**

![Img 1](screenshot/Login.JPG)

**Home**

![Img 2](screenshot/Home.JPG)

**Mata Kuliah**

![Img 3](screenshot/Daftar_Matkul.JPG)
![Img 4](screenshot/Daftar_Matkul2.JPG)

**Daftar Nilai**

![Img 5](screenshot/Daftar_Nilai.JPG)
![Img 6](screenshot/Daftar_Nilai2.JPG)

**Jadwal Kuliah**

![Img 7](screenshot/Jadwal_Kuliah.JPG)
![Img 8](screenshot/Jadwal_Kuliah2.JPG)

**Daftar Dosen**

![Img 9](screenshot/Daftar_Dosen.JPG)
![Img 10](screenshot/Daftar_Dosen2.JPG)

**Daftar Mahasiswa**

![Img 11](screenshot/Daftar_Mahasiswa.JPG)
