<?php 
include_once('library/db_func.php');
include_once('library/controller.php');

//echo basename(__FILE__);
$modul = 'jadwal';
controller(
	array(
		'modul'=>$modul,
		'batasan'=>array('form','simpan','delete'),
		'url'=>"$modul.php",
		'tabel'=>$modul,
		'attribute'=>array(
						'jadwalId'		=>['ispk'=>true,], 
						'jadwalNama'	=>['label'=>'Mata Kuliah'  	,'fungsi'=>'form_input'], 
						'jadwalHari'	=>['label'=>'Hari' 	,'fungsi'=>'form_input'],
						'jadwalKuliah'	=>['label'=>'Pukul' 	,'fungsi'=>'form_input'],
						'jadwalSelesai'	=>['label'=>'Selesai' 	,'fungsi'=>'form_input'],
					),
		'link'=>array(
					array('label'=>'Edit','url'=>"index.php?modul=$modul&act=form"),
					array('label'=>'Delete','url'=>"index.php?modul=$modul&act=delete"),
				),
	)
);

?>