<?php 
include_once('library/db_func.php');
include_once('library/controller.php');

//echo basename(__FILE__);
$modul = 'daftardosen';
controller(
	array(
		'modul'=>$modul,
		'batasan'=>array('form','simpan','delete'),
		'url'=>"$modul.php",
		'tabel'=>$modul,
		'attribute'=>array(
						'dosenId'		=>['ispk'=>true,], 
						'dosenNama'	=>['label'=>'Nama Dosen'  	,'fungsi'=>'form_input'], 
						'dosenNip'	=>['label'=>'NIP Dosen' 	,'fungsi'=>'form_input'],
					),
		'link'=>array(
					array('label'=>'Edit','url'=>"index.php?modul=$modul&act=form"),
					array('label'=>'Delete','url'=>"index.php?modul=$modul&act=delete"),
				),
	)
);

?>