<?php 
include_once('library/db_func.php');
include_once('library/controller.php');

//echo basename(__FILE__);
$modul = 'daftarmhs';
controller(
	array(
		'modul'=>$modul,
		'batasan'=>array('form','simpan','delete'),
		'url'=>"$modul.php",
		'tabel'=>$modul,
		'attribute'=>array(
						'mhsId'		=>['ispk'=>true,], 
						'mhsNama'	=>['label'=>'Nama Mahasiswa'  	,'fungsi'=>'form_input'], 
						'mhsNim'	=>['label'=>'NIM Mahasiswa' 	,'fungsi'=>'form_input'],
						'mhsPro'	=>['label'=>'Program Studi' 	,'fungsi'=>'form_input'],
					),
		'link'=>array(
					array('label'=>'Edit','url'=>"index.php?modul=$modul&act=form"),
					array('label'=>'Delete','url'=>"index.php?modul=$modul&act=delete"),
				),
	)
);

?>