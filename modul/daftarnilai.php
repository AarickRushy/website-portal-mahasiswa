<?php 
include_once('library/db_func.php');
include_once('library/controller.php');

//echo basename(__FILE__);
$modul = 'daftarnilai';
controller(
	array(
		'modul'=>$modul,
		'batasan'=>array('form','simpan','delete'),
		'url'=>"$modul.php",
		'tabel'=>$modul,
		'attribute'=>array(
						'matkulId'		=>['ispk'=>true,], 
						'matkulNama'	=>['label'=>'Mata Kuliah'  	,'fungsi'=>'form_input'], 
						'matkulNilai'	=>['label'=>'Nilai' 	,'fungsi'=>'form_input'],
					),
		'link'=>array(
					array('label'=>'Edit','url'=>"index.php?modul=$modul&act=form"),
					array('label'=>'Delete','url'=>"index.php?modul=$modul&act=delete"),
				),
	)
);

?>