<?php 
include_once("db_func.php");
include_once("login.php");

function controller($param){
	// cek apakah ada/tidak informasi batasan 
	$batasan = (isset($param['batasan'])?$param['batasan']:array());	
	// cek apakah ada/tidak informasi modul, bila tidak gunakan tabel  
	$modul   = (isset($param['modul'])  ?$param['modul']:$param['tabel']);
	// cek get dipindah ke atas 	
	$act = isset($_GET['act'])?$_GET['act']:'index';
	if(!bolehAkses($modul,$act,$batasan)){
		header("Location:index.php?modul=masuk");
	}
	
	$table=$param["tabel"];
	// ambil id 
	$id = (isset($_GET['id'])?$_GET['id']:'');	
	// bila id tidak kosong, maka load data
	$d = array();   // dibuat supaya tidak error di bagian form 
	$url = "?modul=$modul&act=simpan";
	$pk = '';
	$header = array();
	// disini mencari primary key 
	foreach($param["attribute"] as $k=>$y){
		if(isset($y["ispk"]) && $y["ispk"]){
			$pk = $k; 
		}
		if(isset($y["label"]) && $y["label"]){
			$header[] = $y['label'];
		}
	}
	$where = "";
	$title="Tambah ".ucwords($table);
	if($id!==''){
		$url .= "&id=".$id;
		$where = $pk."=".$id;
		$title="Update ".ucwords($table);
		// bila user tidak ada, arahkan juga ke daftar.php 
		$d = select($table,$where);
		if(count($d)===0){
			header("Location: indx.php?modul=$modul");
		}
		// ambil data pertama, karena datanya array
		$d = $d[0];
	}
	// perulangan pembuatan fungsi form:
	$form = function($url,$data,$id,$title,$attribute){
		echo "<h3>$title</h3>";
		echo '<form method="post" action="'.$url.'" enctype="multipart/form-data">';
		foreach($attribute as $k=>$v){
			if(!isset($v['fungsi'])){
				continue;
			}
			//print_r($v);continue;
			$ks = $v['fungsi'];
			echo $ks(
				$v['label'],'data['.$k.']',(isset($data[$k])?$data[$k]:''),
				isset($v['params'])?$v['params']:''
			);
		}
		
		echo form_input('','Simpan','Simpan','submit');
		echo '</form>';
	};
	
	// pembuatan fungsi daftar	
	$daftar = function($param,$header,$pk){
		if(isset($_GET['msg']) && $_GET['msg']!=''){
			?>
			<script>alert('<?php echo $_GET['msg']; ?>');</script>
			<?php 
		} 
		?>
		<h4>Daftar <?php echo $param['modul'];?></h4>
		<a href="index.php?modul=<?php echo $param['modul'];?>&act=form" class="button primary">Tambah</a>
		<hr/>
		<?php 
		$t = isset($param['select']) && $param['select']!=''?$param['select']:$param['tabel'];
		$data = select($t);
		table($header,$data,$param['link'],$pk);
	};

	
	switch($act){
		case 'form'  :	$form($url,$d,$pk,$title,$param["attribute"]);break;
		case 'simpan':  if(isset($_POST['data'])){
							$data = $_POST['data'];
							if(is_array($param['file'])){
								$f = $param['file'];
								$namafile = simpan_file($f['nama_attr'], $f['lokasi_simpan'], $f['filter']);								
								if($namafile!==''){
									$data[$f['nama_attr']] = $namafile;
								}
							}
							save($table,$data,$where);
							header('Location: index.php?modul='.$param['modul'].'&msg=Data berhasil disimpan');
							return;
						}
						header('Location: index.php?modul='.$param['modul'].'&msg=Tidak ada data disimpan');
						break;
		case 'delete':  
						if(is_array($param['file'])){
							$f = $param['file'];
							// jika file ada, hapus, jika tidak jangan
							if(is_file($d[$f['nama_attr']])){
								unlink($d[$f['nama_attr']]);
							}
						}
						hapus($table,$where);
		default:	$daftar($param,$header,$pk);break;
	}
}













