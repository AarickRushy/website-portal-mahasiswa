<?php 
include_once("db_func.php");

function login(){	
	if(isset($_POST['login'])){
		$login = $_POST['login'];
		$a = select("user","userId = '".$login['username']."'");
		
		if(isset($a[0])){
			$a = $a[0];
			if(
				isset($a['userId']) && $a['userId']==$login['username'] &&
				isset($a['userPassword']) && $a['userPassword']==($login['password']) 
			){
				$_SESSION['login'] = $a['userId'];
				$_SESSION['acl'] = array(
							'matkul'=>array('form','simpan','daftar','delete'),
							'daftarnilai'=>array('form','simpan','daftar','delete'),
							'jadwal'=>array('form','simpan','daftar','delete'),
							'daftardosen'=>array('form','simpan','daftar','delete'),
							'daftarmhs'=>array('form','simpan','daftar','delete')
						);
				header('location:index.php');
			}
		}
		echo "<h4>Data tidak ditemukan</h4>";
	}
	
	echo "<h3>Login</h3>";
	echo "<p>Silahkan login menggunakan kredential Anda</p>";
	echo '<form method="post">';
	echo form_input('Nama User','login[username]','','text');
	echo form_input('Password','login[password]','','password');	
	echo form_input('','Simpan','Simpan','submit');
	echo '</form>';
}


function bolehAkses($modul,$aksi,$dibatasi=array()){
	// jika $dibatasi adalah array kosong maka return kan true
	if(empty($dibatasi)){
		return true;
	}
	
	// jika ada tanda bintang atau $aksi, maka cek login
	if(in_array('*',$dibatasi) || in_array(strtolower($aksi),$dibatasi) ){
		//cek login
		if(isset($_SESSION['login']) && isset($_SESSION['acl'][$modul])){
			$acl = $_SESSION['acl'][$modul];
			//kembalikan apakah user memiliki hak akses
			return in_array($aksi,$acl);
		}		
		else{
			// tidak ditemukan session login, kembalikan false 
			return false;
		}
	}
	// tidak dibatasi, kembalikan true
	return true;
}

function logout(){
	session_destroy();
	header('Location: index.php?modul=masuk');
}