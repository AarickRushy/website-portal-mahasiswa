-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 22, 2021 at 09:28 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dataakademik`
--

-- --------------------------------------------------------

--
-- Table structure for table `daftardosen`
--

CREATE TABLE `daftardosen` (
  `dosenId` int(20) NOT NULL,
  `dosenNama` varchar(45) NOT NULL,
  `dosenNip` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `daftardosen`
--

INSERT INTO `daftardosen` (`dosenId`, `dosenNama`, `dosenNip`) VALUES
(4, 'Zakaria Sembiring, S.T.', '19700128 199203 1 002'),
(5, 'Dr. Benny Benyamin Nasution, Dipl. Ing., M. E', '19680809 199702 1 001'),
(6, 'Drs. Anwar', '19590111 198603 1 001'),
(7, 'Friendly, S.T., M.T.', '19810824 201404 1 001'),
(8, 'Aprilza Aswani, SPd., M.A.', '19930427 201903 2 028'),
(9, 'Ir. Zulkifli Lubis, M.I.Komp.', '19571103 198503 1 002'),
(10, 'Gunawan, S.T., M.Kom.', '19750604 200003 1 002'),
(11, 'Marliana Sari, S.T., M,MSI', '19770327 201504 2 002'),
(12, 'Ferry Fachrizal, S.T., M. Kom.', '19740126 200003 1 001'),
(13, 'Achmad Yani, S.T., M.Kom.', '19740127 200003 1 005'),
(14, 'Julham, S.T., M.Kom.', '19761212 200112 1 002');

-- --------------------------------------------------------

--
-- Table structure for table `daftarmhs`
--

CREATE TABLE `daftarmhs` (
  `mhsId` int(20) NOT NULL,
  `mhsNama` varchar(45) NOT NULL,
  `mhsNim` int(20) NOT NULL,
  `mhsPro` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `daftarmhs`
--

INSERT INTO `daftarmhs` (`mhsId`, `mhsNama`, `mhsNim`, `mhsPro`) VALUES
(1, 'Muhammad Taufiq Hidayat', 1805112035, 'Teknik Komputer'),
(2, 'Ade Puan Maulida', 1805112010, 'Teknik Komputer'),
(3, 'Nathanael Atan Tarigan', 1805112030, 'Teknik Komputer');

-- --------------------------------------------------------

--
-- Table structure for table `daftarnilai`
--

CREATE TABLE `daftarnilai` (
  `matkulId` int(20) NOT NULL,
  `matkulNama` varchar(45) NOT NULL,
  `matkulNilai` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `daftarnilai`
--

INSERT INTO `daftarnilai` (`matkulId`, `matkulNama`, `matkulNilai`) VALUES
(1, 'Pemrograman Web (Teori)', 80),
(2, 'Pemrograman Web (Praktik)', 75),
(3, 'Rekayasa Perangkat Lunak', 70),
(5, 'Keamanan Jaringan (Teori)', 70),
(6, 'Keamanan Jaringan (Praktik)', 75),
(7, 'Kewarganegaraan', 80),
(8, 'Komunikasi Data', 75),
(9, 'Proyek Inovasi', 80),
(10, 'Mikrokontroler (Teori)', 75),
(11, 'Mikrokontroler (Praktik)', 78),
(12, 'Bahasa Inggris', 75);

-- --------------------------------------------------------

--
-- Table structure for table `jadwal`
--

CREATE TABLE `jadwal` (
  `jadwalId` int(20) NOT NULL,
  `jadwalNama` varchar(45) NOT NULL,
  `jadwalHari` varchar(45) NOT NULL,
  `jadwalKuliah` time NOT NULL,
  `jadwalSelesai` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `jadwal`
--

INSERT INTO `jadwal` (`jadwalId`, `jadwalNama`, `jadwalHari`, `jadwalKuliah`, `jadwalSelesai`) VALUES
(1, 'Pemrograman Web', 'Senin', '13:05:00', '15:20:00'),
(4, 'Praktik Pemrograman Web', 'Senin', '15:20:00', '17:50:00'),
(5, 'Rekayasa Perangkat Lunak', 'Rabu', '16:20:00', '18:35:00'),
(6, 'Keamanan Jaringan', 'Jumat', '13:50:00', '16:05:00'),
(7, 'Praktik Keamanan Jaringan', 'Jumat', '16:20:00', '18:35:00'),
(8, 'Kewarganegaraan', 'Kamis', '13:05:00', '15:20:00'),
(9, 'Komunikasi Data', 'Selasa', '17:50:00', '18:35:00'),
(10, 'Komunikasi Data', 'Kamis', '16:20:00', '18:35:00'),
(11, 'Bahasa Inggris ', 'Rabu', '14:35:00', '16:05:00'),
(12, 'Bahasa Inggris ', 'Kamis', '15:20:00', '16:05:00'),
(13, 'Proyek Inovasi ', 'Senin', '17:50:00', '18:35:00'),
(14, 'Proyek Inovasi ', 'Rabu', '13:05:00', '14:35:00'),
(15, 'Mikrokontroler', 'Selasa', '13:05:00', '15:20:00'),
(16, 'Praktik Mikrokontroler', 'Selasa', '15:20:00', '17:50:00');

-- --------------------------------------------------------

--
-- Table structure for table `matkul`
--

CREATE TABLE `matkul` (
  `kuliahId` int(20) NOT NULL,
  `kuliahNama` varchar(45) NOT NULL,
  `kuliahDosen` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `matkul`
--

INSERT INTO `matkul` (`kuliahId`, `kuliahNama`, `kuliahDosen`) VALUES
(1, 'Pemrograman Web (Teori/Praktik)', 'Friendly, S.T.,M.T.'),
(2, 'Rekayasa Perangkat Lunak', 'Yulia Fatmi, S.Kom., M.Kom.'),
(3, 'Keamanan Jaringan (Teori/Praktik)', 'Dr. Benny Benyamin Nasution, Dipl. Ing., M.En'),
(4, 'Kewarganegaraan', 'Drs. Anwar'),
(5, 'Komunikasi Data', 'Marliana Sari, S.T., M,MSI'),
(6, 'Proyek Inovasi', 'Arif Ridho Lubis, B.IT., M. Sc.IT.'),
(7, 'Mikrokontroler (Teori/Praktik)', 'Gunawan, S.T., M.Kom.'),
(8, 'Bahasa Inggris', 'Aprilza Aswani, SPd., M.A.');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `userId` varchar(45) NOT NULL,
  `userNama` varchar(80) NOT NULL,
  `userPassword` text NOT NULL,
  `userGroup` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`userId`, `userNama`, `userPassword`, `userGroup`) VALUES
('admin', 'Hidayat', '123456', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `daftardosen`
--
ALTER TABLE `daftardosen`
  ADD PRIMARY KEY (`dosenId`);

--
-- Indexes for table `daftarmhs`
--
ALTER TABLE `daftarmhs`
  ADD PRIMARY KEY (`mhsId`);

--
-- Indexes for table `daftarnilai`
--
ALTER TABLE `daftarnilai`
  ADD PRIMARY KEY (`matkulId`);

--
-- Indexes for table `jadwal`
--
ALTER TABLE `jadwal`
  ADD PRIMARY KEY (`jadwalId`);

--
-- Indexes for table `matkul`
--
ALTER TABLE `matkul`
  ADD PRIMARY KEY (`kuliahId`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`userId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `daftardosen`
--
ALTER TABLE `daftardosen`
  MODIFY `dosenId` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `daftarmhs`
--
ALTER TABLE `daftarmhs`
  MODIFY `mhsId` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `daftarnilai`
--
ALTER TABLE `daftarnilai`
  MODIFY `matkulId` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `jadwal`
--
ALTER TABLE `jadwal`
  MODIFY `jadwalId` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `matkul`
--
ALTER TABLE `matkul`
  MODIFY `kuliahId` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
