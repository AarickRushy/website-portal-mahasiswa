<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Metro 4 -->
    <link rel="stylesheet" href="css/metro-all.css">
  </head>
  <body>
    <div data-role="appbar" data-expand-point="md" class="bg-darkRed fg-white">
		<a href="#" class="brand no-hover">
			<span style="width: 60px;" class="p-2 border bd-dark border-radius">
				m<sup>4</sup>
			</span>
		</a>
		<?php echo $menu;?>
	</div>
	<div class="container" style="margin-top:60px">
			<?php echo $content ?>
	</div>
    <!-- jQuery first, then Metro UI JS -->
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/metro.js"></script>
	<script>
	</script>
  </body>
</html>